#include <iostream>
#include <cstring>
#include <cctype>
#include <fstream>
using namespace std;

// ****************************************************************************
// John Quiruz, August 1 2023, CS162, Program 3
// This program creates a collection of favorite superheroes and their 
// background information such as name, special powers, admired quality, one
// historical fact, and their arch nemesis. The user will have the option to 
// enter a new superhero to the collection, display superhero profiles, and
// save information to an external data file.
// ****************************************************************************
 
// Constants
const int F_NAME {31};
const int HEROES {20};
const int NAME {41};
const int POWER {71};
const int QUALITY {71};
const int FACT {101};
const int ADD {1}; // Menu options start...
const int LOAD {2};
const int FIND {3};
const int DISPLAY {4};
const int SAVE {5};
const int EXIT {6}; // ...options end

// Struct
struct superhero // Superhero catalog
{
	char name[NAME];
	char power[POWER];
	char quality[QUALITY];
	char fact[FACT];
	char nemesis[NAME];
};

// Prototypes
int main_menu();
void welcome();
void read_in(superhero & profile);
void search_catalog(char search_term[], superhero profile[], int & catalog_size);
void add_superheroes(superhero profile[], int & catalog_size);
void display_heroes(superhero profile[], int & catalog_size);
void save_file(superhero profile[], int & catalog_size, int num_loads);
int load_file(superhero profile[], int & catalog_size, int & num_loads);
void goodbye();

int main()
{
	// Variables
	superhero input[HEROES]; // Catalog of Superhero information
	char search_term[NAME];  // This name is used to find existing 
       	int menu_choice {0};     // hero in the catalog
	int catalog_size {0};
	char file_name[F_NAME]; // user types in file name they want 
				// to manipulate
	int num_loads {0}; // Keeps track of number of times a file was
			   // loaded in order to know when to append or not
			   // append new data to existing file when SAVING

	// Welcome
	welcome();

	// Repeat main menu until the user chooses to quit program
	do 
	{
	menu_choice = main_menu(); // Input for menu

	// Add Superhero to the catalog until user chooses not to
	if (menu_choice == ADD)
		add_superheroes(input, catalog_size);

	// Load an existing catalog from a file
	else if (menu_choice == LOAD)
		catalog_size = load_file(input, catalog_size, num_loads);

	// Find Superhero in the catalog by passing in the arguments:
	// search term, catalog size, and catalog array
	else if (menu_choice == FIND)
		search_catalog(search_term, input, catalog_size);

	// Takes the existing catalog array and displays all of it's 
	// contents such as background info for each Superhero
	else if (menu_choice == DISPLAY)
		display_heroes(input, catalog_size);
	
	// Save current catalog into a file for later use
	else if (menu_choice == SAVE)
		save_file(input, catalog_size, num_loads);

	} while (menu_choice != EXIT); // Ends the program

	// Goodbye message
	goodbye();

	return 0;
}



// Functions
// Displays the menu interface and provides the user with options
// to choose from and reads in the option choice from the user
int main_menu()
{
	// Variables
	int menu_choice {0};
	
	// Menu interface
	cout << "---------" << endl;
	cout << "Main menu" << endl;
	cout << "---------";
	cout << "\n[1] - ADD Superhero to the catalog" << endl;
	cout << "[2] - LOAD catalog from external file" << endl;
	cout << "[3] - FIND Superhero in catalog" << endl;
	cout << "[4] - DISPLAY Superhero catalog" << endl;
	cout << "[5] - SAVE catalog to external file" << endl;
	cout << "[6] - EXIT program" << endl;

	cout << "\nEnter your choice: "; // Prompt and read menu choice
	cin >> menu_choice;
	cin.ignore(100, '\n');

	while (menu_choice < 1 || menu_choice > 6) // Input validation
	{                                          
		cout << "Please enter correct input [1 - 6]: ";
		cin >> menu_choice;
		cin.ignore(100, '\n');
	}

	return menu_choice; // Will use the choice to navigate through main
}



// Displays welcome message and information about the program
void welcome()
{
	cout << "\n\nSUPERHERO CATALOG" << endl << endl;
}



// Reads in information about one Superhero, which can be used repeatedly
// to proliferate a collection of Superheroes in a struct
void read_in(superhero & info)
{
	cout << "Adding Superhero(s)..." << endl;

	cout << "Enter Superhero name: ";
	cin.get(info.name, NAME, '\n');
	cin.ignore(100, '\n');

	cout << "Enter " << info.name << "\'s super ability: ";
	cin.get(info.power, POWER, '\n');
	cin.ignore(100, '\n');

	cout << "Enter your favorite quality about " << info.name << ": ";
	cin.get(info.quality, QUALITY, '\n');
	cin.ignore(100, '\n');

	cout << "Enter a historical fact about " << info.name << ": ";
	cin.get(info.fact, FACT, '\n');
	cin.ignore(100, '\n');

	cout << "Enter " << info.name << "\'s arch nemesis: ";
	cin.get(info.nemesis, NAME, '\n');
	cin.ignore(100, '\n');
	cout << endl;
}



// Adds information about Superheroes to the catalog and updates the argument
void add_superheroes(superhero profile[], int & catalog_size)
{
	// Variables 
	char choice {'n'};

	// Add information to the catalog and then ask the user if they
	// want to add more Superheroes
	do {
		cout << endl;
		read_in(profile[catalog_size]);
		cout << "Would you like to add another Superhero?"
		     << "'Y' or 'N': ";
		cin >> choice;
		cin.ignore(100, '\n');
		++catalog_size;
	} while ('Y' == toupper(choice));
	cout << endl;
}



// Displays the information of each Superhero in the catalog
void display_heroes(superhero info[], int & catalog_size)
{
	// Header
	cout << "\nDisplaying Superhero catalog..." << endl;

	// Formatting for displaying Superhero catalog
	if (catalog_size > 0)
	for (int i {0}; i < catalog_size; ++i)
		cout << "\nName: " << info[i].name << endl
		     << "Super ability: " << info[i].power << endl
		     << "Favorite quality: " << info[i].quality << endl
		     << "Historical fact: " << info[i].fact << endl
		     << "Sworn enemy: " << info[i].nemesis << endl;
	else
		cout << "\nThere are no Superheroes in the catalog..." 
		     << endl << endl;
}



void save_file(superhero profile[], int & catalog_size, int num_loads)
{
	// Variables
	ofstream file_out;
	char save_name[NAME];

	// Ask the user for a file name to save the Superhero catalog to
	cout << "Enter the name of the file you want to save to: ";
	cin >> save_name;
	cin.ignore(100, '\n');

	if (num_loads > 0)
		file_out.open(save_name); // Overwrites file location with
					  // program data
	else
		file_out.open(save_name, ios::app); // Does NOT overwrite
						    // contents of file but
						    // instead appends new data

	// Check to see if the program is connected to the file
	if (!file_out)
		cout << "Error!" << endl;
	else
	{ // Writes data in current struct array to external data file
		for (int i {0}; i < catalog_size; ++i)
		{
			file_out << profile[i].name << '~'
				 << profile[i].power << '~'
				 << profile[i].quality << '~'
				 << profile[i].fact << '~'
				 << profile[i].nemesis << endl;
		}
		file_out.close();
		cout << "Saved " << catalog_size << " profiles successfully..."
		     << endl << endl;
	}
}



// Searches through the catalog for the superheroes name 
// and displays their information if there is a match
void search_catalog(char search_term[], superhero profile[], int & catalog_size)
{
	//Variables 
	char superhero_name_temp[NAME];
	bool match {false};
	int match_index {0};

	// Check if the catalog exists
	if (catalog_size <= 0)
		cout << "\nThere is no catalog to search..." << endl << endl;
	else
	{
		// Header
		cout << "\nLooking up Superhero in the catalog..." << endl;

		// Gets name to search the catalog with
		cout << "\nPlease enter the Superhero's name: ";
		cin.get(search_term, NAME, '\n');
		cin.ignore(100, '\n');

		// Iterate through Superhero catalog
		for (int i {0}; i < catalog_size && !match; ++i)
		{
			// Make of copy of the current Superhero name during
			// each iteration and compare to the search term
			strcpy(superhero_name_temp, profile[i].name);
			if (strcmp(search_term, superhero_name_temp) == 0)
			{
				match = true; // Set a match flag when found
				match_index = i; // Keeps index of the 
						 // hero that was found in log
			}
		}

		// Display the found hero for convenience
		if (match)
		{
			cout << "Match found. Displaying "
			     << "Superhero's information..."
		             << "\n\nName: " 
			     << profile[match_index].name << endl
		             << "Super ability: " 
			     << profile[match_index].power << endl
		             << "Favorite quality: " 
			     << profile[match_index].quality << endl
		             << "Historical fact: " 
			     << profile[match_index].fact << endl
		             << "Sworn enemy: " 
			     << profile[match_index].nemesis << endl << endl;
		}
		else
			cout << "\nMatch not found." << endl;
	}
}



// Loads in a text file if the file exists
int load_file(superhero profile[], int & catalog_size, int & num_loads)
{
	// Variables
	ifstream file_in;
	int i = 0;
	char load_name[NAME];
        
	// Checks for existing catalog in order to load in data starting
	// at the correct index. Don't want to overwrite data that already
	// exists
	if (catalog_size > 0)
		i = i + catalog_size;

	// User can type in the file name and the program will
	// find that file to load
	cout << "Please enter file name that you want to load: ";
	cin >> load_name;
	cin.ignore(100, '\n');

	// Load the collection of heroes stored in an external file
	file_in.open(load_name); // Add append ios::app?
	if (!file_in)
		cout << "Error! File is not connected." << endl << endl;
	else
	{
		// Primes the pipe; If no data is read in, an EOF flag 
		// is set and the preceding loop never starts
		file_in.get(profile[i].name, NAME, '~');
		file_in.ignore(100, '~');

		// Reads in data from an external file and saves it to the log
		while (file_in && !file_in.eof() && i < HEROES)
		{
			file_in.get(profile[i].power, POWER, '~');
			file_in.ignore(100, '~');
			file_in.get(profile[i].quality, QUALITY, '~');
			file_in.ignore(100, '~');
			file_in.get(profile[i].fact, FACT, '~');
			file_in.ignore(100, '~');
			file_in.get(profile[i].nemesis, NAME, '\n');
			file_in.ignore(100, '\n');
			++i;
			file_in.get(profile[i].name, NAME, '~');
			file_in.ignore(100, '~');
		}
		file_in.close(); // Closes external file

		// Success message
		cout << "Superhero catalog successfully loaded..." 
		     << endl << endl;
	}
	// If a file was initially loaded in before SAVING, then we
	// know to overwrite the data during a SAVE to avoid duplicating 
	// information in an external file
	++num_loads;
	return i; // Gets assigned to catalog_size in main. Superfluous choice
}



// This function displays a goodbye message before program ends
void goodbye()
{
	cout << "\nTerminating program..." << endl << endl;
	cout << "Thank you for using the Superhero Catalog!" << endl << endl;
}
